package com.n26.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.n26.model.Transaction;
import com.n26.model.TransactionStatistic;
import com.n26.service.TransactionStatisticService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(TransactionController.class)
public class TransactionControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private TransactionStatisticService transactionService;
    @Test
    public void saveTransaction() throws Exception {

        final ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();

        /*final LocalDateTime localTime = LocalDateTime.of(2018, 12, 22, 11, 55, 50, 232);
        final ZonedDateTime zdt = localTime.atZone(ZoneId.systemDefault());

        System.out.println("===================");
        System.out.println(zdt.toInstant().toEpochMilli());
        final int second = LocalDateTime
                .ofInstant(Instant.ofEpochMilli(1545472552000L), ZoneId.systemDefault()).getSecond();
        System.out.println(second);
        System.out.println(System.currentTimeMillis());
        System.out.println("=========================" + currentIndexFor(1545472552000L));*/


        final Transaction transaction = new Transaction(BigDecimal.valueOf(232.43d), ZonedDateTime.now(ZoneOffset.UTC));

        TransactionStatistic transactionStatistic = new TransactionStatistic();
        transactionStatistic= transactionStatistic.register(transaction.getAmount());
        mockMvc.perform(post("/transactions")
                .contentType(MediaType.APPLICATION_JSON)
                .content(ow.writeValueAsString(transaction)))
                .andDo(print())
                .andExpect(status().isCreated());
    }
}