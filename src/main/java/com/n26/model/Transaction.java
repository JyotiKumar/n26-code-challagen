package com.n26.model;


import com.n26.annonation.ValidTransactionTime;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Transaction {

    /**
     * A string of arbitrary length that is parsable as a BigDecimal
     */
    @NotNull
    private BigDecimal amount;

    /**
     * transaction time in the ISO 8601 format YYYY-MM-DDThh:mm:ss.sssZ in the UTC timezone
     */
    @NotNull
    @ValidTransactionTime(timeFrame = 60, unit = ChronoUnit.SECONDS, message = "timestamp is older than 60 seconds")
    private ZonedDateTime timestamp;


}
