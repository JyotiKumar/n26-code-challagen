package com.n26.model;


import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;

@Data
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@Slf4j
public class TransactionStatistic {

    // FIXME  the default round half can be done in more cleaner way

    @JsonFormat(shape = JsonFormat.Shape.STRING)
    @Builder.Default
    private BigDecimal sum = BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_HALF_UP);
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    @Builder.Default
    BigDecimal avg = BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_HALF_UP);
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    @Builder.Default
    private BigDecimal max = BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_HALF_UP);
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    @Builder.Default
    private BigDecimal min = BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_HALF_UP);
    private long count;

    // FIXME can me move somewhere else
    public TransactionStatistic register(BigDecimal amount) {
        return TransactionStatistic.builder()
                .count(1)
                .sum(amount)
                .min(amount)
                .max(amount)
                .build();
    }

    // FIXME can me move somewhere else
    public TransactionStatistic merge(TransactionStatistic statistic) {
        if (this == null || statistic == null) {
            return TransactionStatistic.builder().build();
        }
     /*   System.out.println("=========================================");
        System.out.println("statistic = [" + statistic + "]");
        System.out.println("statistic = [" + this + "]");
        System.out.println("=========================================");*/
        TransactionStatistic transactionStatistic = TransactionStatistic.builder()
                .sum(this.getSum().add(statistic.getSum()))
                .max(this.getMax().max(statistic.getMax()))
                .min(this.getMin().min(statistic.getMin()))
                .count(this.getCount() + statistic.getCount())
                .build();
        return transactionStatistic;

    }

}
