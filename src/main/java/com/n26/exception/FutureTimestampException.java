package com.n26.exception;

public class FutureTimestampException  extends IllegalArgumentException {
    public FutureTimestampException(String message) {
        super(message);
    }

    public static void check(boolean condition, String message, Object... args) {
        if (!condition) {
            throw new FutureTimestampException(String.format(message, args));
        }
    }
}
