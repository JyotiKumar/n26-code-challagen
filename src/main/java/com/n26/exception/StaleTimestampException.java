package com.n26.exception;

public class StaleTimestampException extends IllegalArgumentException {
    public StaleTimestampException(String message) {
        super(message);
    }

    public static void check(boolean condition, String message, Object... args) {
        if (!condition) {
            throw new StaleTimestampException(String.format(message, args));
        }
    }
}
