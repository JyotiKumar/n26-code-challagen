package com.n26.controller.handler;


import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.n26.controller.TransactionController;
import com.n26.exception.FutureTimestampException;
import com.n26.exception.StaleTimestampException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.NO_CONTENT;
import static org.springframework.http.HttpStatus.UNPROCESSABLE_ENTITY;


@ControllerAdvice(assignableTypes = TransactionController.class)
@Slf4j
public class ExceptionHandlerControllerAdvice {


    @ExceptionHandler({MethodArgumentNotValidException.class, HttpMessageNotReadableException.class})
    @ResponseStatus(BAD_REQUEST)
    public void handleInValidRequestException() {
        log.warn("Bad request");
    }

    @ExceptionHandler(StaleTimestampException.class)
    @ResponseStatus(NO_CONTENT)
    public void handleStaleTimestampException() {

    }

    @ExceptionHandler(FutureTimestampException.class)
    @ResponseStatus(UNPROCESSABLE_ENTITY)
    public void handleInValidTransactionTimeException() {

    }


    @ExceptionHandler(InvalidFormatException.class)
    @ResponseStatus(UNPROCESSABLE_ENTITY)
    public void handelBadRequestException() {

    }

}
