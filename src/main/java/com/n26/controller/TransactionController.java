package com.n26.controller;


import com.n26.model.Transaction;
import com.n26.service.TransactionStatisticService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

import static org.springframework.http.HttpStatus.CREATED;

@RestController
@Slf4j
public class TransactionController {

    private final TransactionStatisticService transactionStatisticService;

    @Autowired
    public TransactionController(TransactionStatisticService transactionStatisticService) {
        this.transactionStatisticService = transactionStatisticService;
    }

    @PostMapping("/transactions")
    public ResponseEntity<Void> saveTransaction(@Valid @RequestBody final Transaction transaction) {
        log.info(transaction.toString());
        transactionStatisticService.register(transaction);
        return new ResponseEntity<>(CREATED);
    }

    @DeleteMapping("/transactions")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteTransactions() {
        if(transactionStatisticService!= null)
            transactionStatisticService.clear();
    }
}
