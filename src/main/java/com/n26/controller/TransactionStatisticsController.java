package com.n26.controller;

import com.n26.model.TransactionStatistic;
import com.n26.service.TransactionStatisticService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TransactionStatisticsController {


    private final TransactionStatisticService transactionStatisticService;

    @Autowired
    public TransactionStatisticsController(TransactionStatisticService transactionStatisticService) {
        this.transactionStatisticService = transactionStatisticService;
    }

    @GetMapping("/statistics")
    public TransactionStatistic getTransactionStatistics() {
        return transactionStatisticService.getTransactionStatistic();
    }
}
