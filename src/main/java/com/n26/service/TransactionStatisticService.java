package com.n26.service;

import com.n26.model.Transaction;
import com.n26.model.TransactionStatistic;

public interface TransactionStatisticService {

    void register(Transaction transaction);
    TransactionStatistic getTransactionStatistic();
    void clear();
}
