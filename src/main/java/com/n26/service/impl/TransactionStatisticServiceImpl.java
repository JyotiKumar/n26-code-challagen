package com.n26.service.impl;

import com.n26.model.Transaction;
import com.n26.model.TransactionStatistic;
import com.n26.service.TransactionStatisticService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantReadWriteLock;

@Service
@Slf4j
public class TransactionStatisticServiceImpl implements TransactionStatisticService {

    private static final int DURATION_STATS = 60;
    private static final Map<AtomicInteger, TransactionStatistic> transactionStore = new ConcurrentHashMap<>(DURATION_STATS);

    @Override
    public void register(Transaction transaction) {
        System.out.println("before saving " + transactionStore.size());
        int nanoSecond = LocalDateTime
                .ofInstant(Instant.ofEpochMilli(transaction.getTimestamp().toInstant().toEpochMilli()), ZoneId.of(ZoneOffset.UTC.getId())).getNano();


        transactionStore.compute(new AtomicInteger(nanoSecond), (k, v) -> {
            TransactionStatistic transactionStatistic = new TransactionStatistic();
            if (v == null) {
                return transactionStatistic.register(transaction.getAmount());
            }
            transactionStatistic = transactionStatistic.register(transaction.getAmount());
            return transactionStatistic.merge(transactionStatistic);
        });
        //transactionStore.forEach((k, v) -> System.out.println("Item : " + k + " Count : " + v));
        System.out.println("after saving " + transactionStore.size());
    }


    // FIXME this statics is not working as the stale transactions are not removed automatically for the list

    /**
     * This error comes while executing the http03.json request file.
     *     org.junit.ComparisonFailure: Response Json does not match with the expected Json
     * Expected :{"sum":"1626.62","avg":"47.84","max":"89.42","min":"2.11","count":34}
     * Actual   :{"sum":"1869.55","avg":"46.74","max":"89.42","min":"2.11","count":40}
     *
     *
     *
     * TODO: Find a nice and handy way to remove the old transaction, before returning the transaction
        statistics hy having the current time stamp in the data store map.
     *
     *     */

    @Override
    public TransactionStatistic getTransactionStatistic() {

        System.out.println("The store size for getTransactionStatistic  " + transactionStore.size());
        //transactionStore.forEach((k, v) -> System.out.println("Item : " + k + " Count : " + v));
        final Optional<TransactionStatistic> transactionStatistic = transactionStore.values().parallelStream().reduce(TransactionStatistic::merge);
        if (transactionStatistic.isPresent()) {
            return doRoundUpBigDecimalValue(transactionStatistic.get());
        } else {
            return TransactionStatistic.builder().build();
        }
    }

    @Override
    public void clear() {
        System.out.println("The store size before clear " + transactionStore.size());
        ReentrantReadWriteLock writeLock = new ReentrantReadWriteLock();
        writeLock.writeLock().lock();
        try {
            transactionStore.clear();
        } finally {
            writeLock.writeLock().unlock();
        }

    }

    //FIXME
    private TransactionStatistic doRoundUpBigDecimalValue(TransactionStatistic transactionStatistic) {
        final BigDecimal avg = transactionStatistic.getSum().divide(BigDecimal.valueOf(transactionStatistic.getCount()), 2, BigDecimal.ROUND_HALF_UP);
        System.out.println("Average = [" + avg + "]");
        return transactionStatistic.toBuilder()
                .sum(transactionStatistic.getSum().setScale(2, BigDecimal.ROUND_HALF_UP))
                .max(transactionStatistic.getMax().setScale(2, BigDecimal.ROUND_HALF_UP))
                .min(transactionStatistic.getMin().setScale(2, BigDecimal.ROUND_HALF_UP))
                .count(transactionStatistic.getCount())
                .avg(avg)
                .build();

    }
}
