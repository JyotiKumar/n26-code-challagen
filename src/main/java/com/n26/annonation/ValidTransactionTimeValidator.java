package com.n26.annonation;


import com.n26.exception.FutureTimestampException;
import com.n26.exception.StaleTimestampException;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.time.Duration;
import java.time.Instant;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;

public class ValidTransactionTimeValidator implements ConstraintValidator<ValidTransactionTime, ZonedDateTime> {

    private Duration timeDuration;

    @Override
    public void initialize(final ValidTransactionTime constraintAnnotation) {
        timeDuration = Duration.of(constraintAnnotation.timeFrame(), constraintAnnotation.unit());
    }

    @Override
    public boolean isValid(final ZonedDateTime transactionTime, final ConstraintValidatorContext constraintValidatorContext) {
        if (isFutureTransactionDate(transactionTime)) { // check if the time stamp is in future
            throw new FutureTimestampException("Transaction is future time");
        } else if (isStaleTransaction(transactionTime)) {  // check if the time stamp is in 60 sec
            throw new StaleTimestampException("Time stamp is not valid");
        }
        return true;
    }

    private boolean isFutureTransactionDate(ZonedDateTime transactionTime) {
        return ChronoUnit.MILLIS.between(transactionTime, Instant.now().atOffset(ZoneOffset.UTC)) < 0;
    }

    private boolean isStaleTransaction(ZonedDateTime transactionTime) {
        return ChronoUnit.MILLIS.between(transactionTime, Instant.now().atOffset(ZoneOffset.UTC)) > timeDuration.toMillis();
    }
}
