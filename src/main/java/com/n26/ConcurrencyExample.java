/*
package com.n26;
import java.io.PrintStream;
import java.text.DecimalFormat;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.BiFunction;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class ConcurrencyExample {

    interface TemporaryStorage<K, V> {

        V compute(K key, BiFunction<? super K, ? super V, ? extends V> remapper);

        V put(K key, V value);

        V get(K key);

        void clear();

        @FunctionalInterface
        interface UnitTest<K, V> {

            void test(TemporaryStorage<K, V> store, K key);

        }

    }

    static class ConcurrentHashMapTS<K, V> implements TemporaryStorage<K, V> {

        private final Map<K, V> map = new ConcurrentHashMap<>();

        @Override
        public V compute(K key, BiFunction<? super K, ? super V, ? extends V> remapper) {
            return map.compute(key, remapper);
        }

        @Override
        public V put(K key, V value) {
            return map.put(key, value);
        }

        @Override
        public V get(K key) {
            return map.get(key);
        }

        @Override
        public void clear() {
            map.clear();
        }
    }

    static class AtomicReferenceHashMapTS<K, V> implements TemporaryStorage<K, V> {

        private final AtomicReference<Map<K, V>> map = new AtomicReference<>(new HashMap<>());

        @Override
        public V compute(K key, BiFunction<? super K, ? super V, ? extends V> remapper) {
            return map.get().compute(key, remapper);
        }

        @Override
        public V put(K key, V value) {
            return map.get().put(key, value);
        }

        @Override
        public V get(K key) {
            return map.get().get(key);
        }

        @Override
        public void clear() {
            map.get().clear();
        }
    }

    static class MonitorLockedHashMapTS<K, V> implements TemporaryStorage<K, V> {

        private final Map<K, V> map = new HashMap<>();
        private final Object mutex = new Object(); // could use the map as the mutex

        @Override
        public V compute(K key, BiFunction<? super K, ? super V, ? extends V> remapper) {
            synchronized (mutex) {
                return map.compute(key, remapper);
            }
        }

        @Override
        public V put(K key, V value) {
            synchronized (mutex) {
                return map.put(key, value);
            }
        }

        @Override
        public V get(K key) {
            synchronized (mutex) {
                return map.get(key);
            }
        }

        @Override
        public void clear() {
            synchronized (mutex) {
                map.clear();
            }
        }
    }

    static class WrappedHashMapTS<K, V> implements TemporaryStorage<K, V> {

        private final Map<K, V> map = Collections.synchronizedMap(new HashMap<>());

        @Override
        public V compute(K key, BiFunction<? super K, ? super V, ? extends V> remapper) {
            return map.compute(key, remapper);
        }

        @Override
        public V put(K key, V value) {
            return map.put(key, value);
        }

        @Override
        public V get(K key) {
            return map.get(key);
        }

        @Override
        public void clear() {
            map.clear();
        }
    }

    static class AtomicUnitTest implements TemporaryStorage.UnitTest<Integer, Integer> {

        @Override
        public void test(TemporaryStorage<Integer, Integer> store, Integer key) {
            store.compute(key, (k, v) -> (v == null ? 0 : v) + 1);
        }
    }

    static class UnsafeUnitTest implements TemporaryStorage.UnitTest<Integer, Integer> {

        @Override
        public void test(TemporaryStorage<Integer, Integer> store, Integer key) {
            Integer value = store.get(key);
            store.put(key, (value == null ? 0 : value) + 1);
        }
    }

    public static class TestRunner {

        public static void main(String... args) throws InterruptedException {
            final int iterations = 1_000;
            final List<Integer> keys = IntStream.rangeClosed(1, iterations).boxed().collect(Collectors.toList());

            final int expected = iterations;

            for (int batch = 1; batch <= 5; batch++) {

                System.out.println(String.format("--- START BATCH %d ---", batch));

                test(System.out, new ConcurrentHashMapTS<>(), new AtomicUnitTest(), keys, expected, iterations);
                test(System.out, new ConcurrentHashMapTS<>(), new UnsafeUnitTest(), keys, expected, iterations);

            */
/*    test(System.out, new AtomicReferenceHashMapTS<>(), new AtomicUnitTest(), keys, expected, iterations);
                test(System.out, new AtomicReferenceHashMapTS<>(), new UnsafeUnitTest(), keys, expected, iterations);

                test(System.out, new MonitorLockedHashMapTS<>(), new AtomicUnitTest(), keys, expected, iterations);
                test(System.out, new MonitorLockedHashMapTS<>(), new UnsafeUnitTest(), keys, expected, iterations);

                test(System.out, new WrappedHashMapTS<>(), new AtomicUnitTest(), keys, expected, iterations);
                test(System.out, new WrappedHashMapTS<>(), new UnsafeUnitTest(), keys, expected, iterations);
*//*

                System.out.println(String.format("--- END   BATCH %d ---", batch));

                System.out.println();

            }
        }

        private static <K, V> void test(PrintStream printer, TemporaryStorage<K, V> store, TemporaryStorage.UnitTest<K, V> work, List<K> keys, V expected, int iterations) throws InterruptedException {
            test(printer, store, work, keys, expected, iterations, Runtime.getRuntime().availableProcessors() * 4);
        }

        private static <K, V> void test(PrintStream printer, TemporaryStorage<K, V> store, TemporaryStorage.UnitTest<K, V> work, List<K> keys, V expected, int iterations, int parallelism) throws InterruptedException {
            final ExecutorService workers = Executors.newFixedThreadPool(parallelism);
            final long start = System.currentTimeMillis();

            for (K key : keys) {
                for (int iteration = 1; iteration <= iterations; iteration++) {

                    workers.execute(() -> {
                        try {
                            work.test(store, key);
                        } catch (Exception e) {
                            //e.printStackTrace(); //thrown by the AtomicReference<Map<K, V>> implementation
                        }
                    });

                }
            }

            workers.shutdown();
            workers.awaitTermination(Long.MAX_VALUE, TimeUnit.DAYS);

            final long finish = System.currentTimeMillis();

            final DecimalFormat formatter = new DecimalFormat("###,###");

            final long correct = keys.stream().filter(key -> expected.equals(store.get(key))).count();

            printer.println(String.format("Store '%s' performed %s iterations of %s across %s threads in %sms. Accuracy: %d / %d (%4.2f percent)", store.getClass().getSimpleName(), formatter.format(iterations), work.getClass().getSimpleName(), formatter.format(parallelism), formatter.format(finish - start), correct, keys.size(), ((double) correct / keys.size()) * 100));
        }

    }

}*/
